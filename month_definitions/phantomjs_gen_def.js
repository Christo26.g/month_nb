// Name: phamtomjs_gen_def.js
// SPDX-FileCopyrightText: 2013-2021 Simon Descarpentres <simon /\ acoeuro [] com>, 2021 Christopher Gauthier <Christopher /\ christopher-gauthier [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals require, phantom */
// console.log (new Date ().toLocaleString () + ' Starting')
var page = require('webpage').create()
var fs = require ('fs')

page.settings.loadImages = false
page.settings.webSecurityEnabled = false
page.onConsoleMessage = function (msg) { console.log (msg) }
//page.onLoadFinished = function () { console.log (timestamp() + ' load finished '+page.url) }
//page.onUrlChanged = function () { console.log (timestamp () + ' URL Changed '+page.url) }
//page.onNavigationRequested = function(url, type, willNavigate, main) { };*/
//page.onResourceReceived = function(response) { };*/
page.onError = function(msg, trace) { // return
	var msgStack = [new Date ().toLocaleString () + ' ERROR: ' + msg]
	if (trace && trace.length) {
		msgStack.push('TRACE:')
		trace.forEach(function(t) {
			msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' +
				t.function + '")' : ''))
		})
	}
	console.error (msgStack.join('\n'))
	page.onCallback = null
	// ends ('page.onError', u_idx, page)
}
// Appendix:Months_of_the_Year
var the_page_content = fs.read ('Appendix:Months_of_the_year')
page.setContent(the_page_content, 'Generating JSON definitions of month numbers')
page.injectJs('in_browser_gen_def.js')
phantom.exit ()
