// Name: in_browser_gen_def.js
// SPDX-FileCopyrightText: 2013-2021 Simon Descarpentres <simon /\ acoeuro [] com>, 2021 Christopher Gauthier <Christopher /\ christopher-gauthier [] com>
// SPDX-License-Identifier: GPL-3.0-only
var tbx = document.querySelectorAll ('#mw-content-text tr')
var mns = [], lgs = []
var cur_mns
for (let i = tbx.length; i--; ) {
	// console.log (tbx[i].querySelector ('td').textContent)
	mns.push ([])
	cur_mns = tbx[i].querySelectorAll ('td')
	lgs.push (cur_mns[0].textContent.replace(/\n/g, ''))
	for (let j = cur_mns.length; j--; ) {
		if (j != 0) mns[mns.length-1].unshift (cur_mns[j])
	}
	//	console.log (lgs[lgs.length-1] + ' ' + mns[mns.length-1].length)
}
// console.log(`parsed langs : ${lgs.length/2}`)
for (let i = mns.length / 2; i--; )
	for (let j = mns[i].length; j--; )
		mns[i].unshift (mns [i + mns.length / 2][j])
var cur_mn_name, cur_lg_str
var lg_count = mns.length / 2
console.log('{')
for (let i = lg_count; i--; ) {
	cur_lg_str = `"${lgs[i]}":[`
	for (var j = 0; j < mns[i].length; j++) {
		cur_mn_name = mns[i][j].textContent.replace(/\n/g, '')
		cur_mn_name = cur_mn_name.replace (/\(bulan\) /, '')
		cur_lg_str += `"${cur_mn_name}"`
		if (j < 11)
			cur_lg_str += ','
	}
	cur_lg_str += ']'
	if (i > 0)
		cur_lg_str += ','
	console.log(cur_lg_str)
}
console.log('}')
