// Name: test_month_nb.mjs
// SPDX-FileCopyrightText: 2013-2021 Simon Descarpentres <simon /\ acoeuro [] com>, 2021 Christopher Gauthier <Christopher /\ christopher-gauthier [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals process */
'use strict'

import {month_nb} from "./month_nb.js"
import month_nb_json_default from "./month_nb.json"
import month_definitions from "./month_definitions/2019_definitions.json"

const month_nb_json = month_nb_json_default
var langs = Object.keys(month_definitions)
var month_names = []
var broken_lang = ['Abkhaz', 'Bashkir', 'Buryat', 'Cherokee', 'Chuvash', 'Erzya', 'Hawaiian',
	'Kalmyk', 'Lezgi', 'Moksha', 'Navajo', 'Ojibwe', 'Old English', 'Old High German',
	'Old Norse', 'Tibetan', 'Udmurt', 'Uyghur', 'Veps', 'Welsh', 'Yakut',
	'Yiddish', 'Zazaki', 'Sinhalese']
var multiple_lang = ['Arabic 2','Arabic 3','Arabic 4','Persan 2','Italian 2','Russian 2','Polish 2']
var failed_lang = []
var working_lang_nb = 0
for (let lang of langs) {
	if (broken_lang.indexOf(lang) > -1)
		continue
	month_names = month_definitions[lang]
	let short_month_names = []
	for (let a of month_names)
		short_month_names.push(a.slice(0,3))
	console.log(lang, ':', short_month_names.join(' '))
	for (let a = 0; a < lang.length + 2; a++)
		process.stdout.write(' ')
	var month_index = 0
	var month_value = 0
	var month_name = ''
	var month_nb_res = 0
	while(month_index < 12) {
		month_name = month_names[month_index]
		month_value = month_index + 1
		month_nb_res = month_nb(month_name, month_nb_json)
		if (!(month_nb_res == month_value)) {
			console.log(`${month_name} gave ${month_nb_res} instead of ${month_value}`)
			failed_lang.push(lang)
			broken_lang.push(lang)
			break
		}
		process.stdout.write(' ' + (month_nb_res < 10 ? ' ' : '') + String(month_nb_res) + ' ')
		month_index += 1
		if (month_index == 12) {
			working_lang_nb += 1
			process.stdout.write('\n')
		}
	}
}
working_lang_nb = working_lang_nb - multiple_lang.length
console.log(`Working lang. : ${working_lang_nb}`)
console.log(`Broken lang. : ${broken_lang.length}`)
console.log(`List of broken lang : ${broken_lang.join(', ')}`)
console.log(`Failed : ${failed_lang.join(', ')}`)
var to_repair = failed_lang
to_repair.splice(to_repair.indexOf('Croatian'),1) // Croatian : _listopad_ fails to return 10 as it is 11 in Czech and Polish
to_repair.splice(to_repair.indexOf('Slovenian'),1) // Slovenian : _prosinec_ fails to return 1, as it is 12 in Croatian and Czech
console.log(`To repair : ${to_repair.join(', ')}`)
console.log(`Total : ${working_lang_nb + broken_lang.length}`)
